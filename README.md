evakare design, manufacture and distribute independent living aids all over the globe. Our product range has been created around our core values of style, innovation & affordability.

Website : http://www.evekare.com
